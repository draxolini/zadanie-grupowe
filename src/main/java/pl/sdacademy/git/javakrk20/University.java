package pl.sdacademy.git.javakrk20;

import java.util.ArrayList;
import java.util.List;

/*
This class should be encapsulated and immutable
Additionally it requires to have toString() method,
similarly as equals(Object) and hashcode().
 */

public class University {
    public String name;
    public List<Register> registers = new ArrayList<>();

    /*
    This class should be completely deeply immutable except remove() method.
    And maybe some almost obligatory methods should be in there?
    Register should also have keeper that is also a person.
    And this class shouldn't be inner, should it?
    */
    public static class Register {
        private String name;
        private List<Person> students = new ArrayList<>();

        public Register(String name) {
            this.name = name;
        }

        public void addStudent(Person student) {
            students.add(student);
        }

        public List<Person> getStudents() {
            return students;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setStudents(List<Person> students) {
            this.students = students;
        }
    }
}
